import { Component, OnInit } from '@angular/core';
import * as webSkills from 'assets/webSkills.json';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  dataset: any = webSkills;

  ngOnInit() {}
}
