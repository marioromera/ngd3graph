import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SunburstGraphComponent } from './sunburst-graph/sunburst-graph.component';

@NgModule({
  declarations: [AppComponent, SunburstGraphComponent],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
