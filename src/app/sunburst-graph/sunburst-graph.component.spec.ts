import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SunburstGraphComponent } from './sunburst-graph.component';

describe('SunburstGraphComponent', () => {
  let component: SunburstGraphComponent;
  let fixture: ComponentFixture<SunburstGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SunburstGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SunburstGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
