import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core'
import { partition as d3Partition, hierarchy as d3Hierarchy } from 'd3-hierarchy'
import { arc as d3Arc } from 'd3-shape'

import { scaleSequential as d3ScaleSequential } from 'd3-scale'
import { interpolateRainbow as rainbowInterpolator } from 'd3-scale-chromatic'
import { event as d3Event, select as d3Select } from 'd3-selection'
import { zoom as d3Zoom, zoomIdentity as d3ZoomIdentity } from 'd3-zoom'

@Component({
  selector: 'app-sunburst-graph',
  templateUrl: './sunburst-graph.component.html',
  styleUrls: ['./sunburst-graph.component.css']
})
export class SunburstGraphComponent implements OnInit {
  constructor() {}
  @Input() data: any

  @Input() svgWidth = 550
  @Input() svgHeight = 440
  radius = Math.min(this.svgWidth, this.svgHeight) / 2
  hoveredNode: any
  hoveredAncestors = []
  percentageText: string
  endLabel: string
  fullOpacity = 0.8
  unfocusOpacity = 0.3
  @ViewChild('svg') svgRef: ElementRef
  @ViewChild('chartContainer') svgGRef: ElementRef

  zoom = d3Zoom()
    .scaleExtent([1, 10])
    .on('zoom', () => {
      d3Select(this.svgGRef.nativeElement).attr('transform', d3Event.transform)
    })
  // Breadcrumb dimensions: width, height, spacing, width of tip/tail.
  b = {
    w: 75,
    h: 30,
    s: 3,
    t: 10
  }
  // Total size of all segments; we set this later, after loading the data.
  totalSize = 0
  // Mapping of step names to colors.
  colors: any

  partition = d3Partition().size([2 * Math.PI, this.radius])

  root: any
  nodes: any

  arc = d3Arc()
    .startAngle(function(d) {
      return d.x0
    })
    .endAngle(function(d) {
      return d.x1
    })
    .innerRadius(function(d) {
      return d.y0
    })
    .outerRadius(function(d) {
      return d.y1
    })

  ngOnInit() {
    this.root = d3Hierarchy(this.data)
      .sum(function(d) {
        return d.size
      })
      .sort(function(a, b) {
        return b.value - a.value
      })

    this.nodes = this.partition(this.root)
      .descendants()
      .filter(function(d) {
        return d.x1 - d.x0 > 0.005 // 0.005 radians = 0.29 degrees
      })
    this.totalSize = this.nodes[0].value
    this.colors = d3ScaleSequential()
      .domain([0, this.nodes.length])
      .interpolator(rainbowInterpolator)

    d3Select(this.svgGRef.nativeElement)
      .call(this.zoom)
      .call(this.zoom.transform, d3ZoomIdentity.translate(this.svgWidth / 2, this.svgHeight / 3))
  }

  getColor(d, i) {
    return this.colors(d.value * d.depth)
  }

  getOpacity(node) {
    if (this.hoveredNode) {
      const nodeRelated = this.hoveredNode.name === node.data.name || this.hoveredAncestors.indexOf(node) !== -1

      return nodeRelated ? this.fullOpacity : this.unfocusOpacity
    }
    return this.fullOpacity
  }

  handleMouseOver(e, d) {
    e.preventDefault()

    this.hoveredNode = d
    this.hoveredAncestors = this.hoveredNode.ancestors().reverse()
    this.hoveredAncestors.shift()

    const percentage = (100 * d.value) / this.totalSize
    this.percentageText = percentage.toPrecision(3) + '%'
    if (percentage < 0.1) {
      this.percentageText = '< 0.1%'
    }
  }

  handleMouseOut(e) {
    e.preventDefault()
    this.hoveredNode = null
    this.percentageText = ''
    this.hoveredAncestors = []
  }

  zoomTo(e: MouseEvent, container: SVGGElement) {
    d3Select(container).call(this.zoom)
  }

  breadcrumbPoints(d: any, i: number) {
    const points = []
    points.push('0,0')
    points.push(this.b.w + ',0')
    points.push(this.b.w + this.b.t + ',' + this.b.h / 2)
    points.push(this.b.w + ',' + this.b.h)
    points.push('0,' + this.b.h)
    if (i > 0) {
      // Leftmost breadcrumb; don't include 6th vertex.
      points.push(this.b.t + ',' + this.b.h / 2)
    }
    return points.join(' ')
  }
}
